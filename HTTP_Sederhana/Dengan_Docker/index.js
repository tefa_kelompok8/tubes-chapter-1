const express = require('express');
const redis = require('redis');

const app = express();
const client = redis.createClient({
    host: 'redis-server',
});

app.get('/', (req, res) => {
    res.send('hello world');
});


app.listen(9001, () => {
    console.log('Listen on port 9001');
});