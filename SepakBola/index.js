// Memanggil module export
const Pemain = require('./pemain');

// Deklarasi fungsi main
function main() {
  // Membuat Objek player dan mengisi nilai constructor
  let player = new Pemain('Budi', 09, 'Striker', 100_000_000_000);

  // Menampilkan data dengan console.log
  console.log('Data Pemain : \nNama:', player.nama, '\nNo Punggung:', player.noPunggung, '\nPosisi:', player.posisi, '\nGaji:', player.gaji, 'Rupiah');
  console.log('\nMari kita beralih ke pertandingannya');

  // Memanggil dan Mengisi Method
  player.passing('Mamat');
  player.dribbling('Wawan');
  player.shooting('Cahyono');
  player.gol(3);
}

// Menjalankan fungsi main
main();
